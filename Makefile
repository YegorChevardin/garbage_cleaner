CC = clang
CFLAGS = -g -O0
SOURCES = main.c
SOURCES_SRC = src
EXECUTABLE = main.bin
EXECUTABLE_SRC = dist

all: format clean prep compile
	./$(EXECUTABLE_SRC)/$(EXECUTABLE)
format: $(SOURCES_SRC)/.clang-format
	clang-format $(SOURCES_SRC)/$(SOURCES) -i
clean:
	rm -rf $(EXECUTABLE_SRC)
prep:
	mkdir $(EXECUTABLE_SRC)
compile: $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES_SRC)/$(SOURCES)
	$(CC) $(CFLAGS) $< -o $(EXECUTABLE_SRC)/$@
